package io.captainsly.gems.lib;

public final class LibGuiId {

	public static final int SCORCHING_OVEN = 0;
	public static final int FUSING_FURNACE = 1;
	
}
