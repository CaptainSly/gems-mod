package io.captainsly.gems.lib;

public final class LibMisc {

	public static final String MOD_ID = "gems";
	public static final String MOD_NAME = "Gems";
	public static final String VERSION = "1.0";
	
	public static final String CLIENT_PROXY = "io.captainsly.gems.proxy.ClientProxy";
	public static final String COMMON_PROXY = "io.captainsly.gems.proxy.CommonProxy";
}
