package io.captainsly.gems.lib;

import net.minecraft.item.Item.ToolMaterial;
import net.minecraftforge.common.util.EnumHelper;

public final class LibMaterials {

	public static final ToolMaterial SAPPHIRE_TOOL = EnumHelper.addToolMaterial("SAPPHIRE", 2, 340, 6.35F, 2.012F, 14);
	public static final ToolMaterial RUBY_TOOL = EnumHelper.addToolMaterial("RUBY", 2, 340, 6.20F, 2.001F, 14);

}
