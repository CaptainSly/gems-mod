package io.captainsly.gems.lib;

import net.minecraft.util.ResourceLocation;

public final class LibRes {

	public static final ResourceLocation SCORCHING_OVEN_GUI = new ResourceLocation(LibMisc.MOD_ID + ":" + "textures/gui/GuiScorchingOven.png");
	
}
