package io.captainsly.gems.items.gems;

import io.captainsly.gems.GemsCreativeTab.GemsCreativeTabGem;
import net.minecraft.item.Item;

public class GemRuby extends Item {
	
	public GemRuby() {
		setUnlocalizedName("rubyGem");
		// TODO: Create a ruby texture
		setTextureName("ruby"); // Uses the Minecraft ruby png. WILL CHANGE
		setCreativeTab(GemsCreativeTabGem.instance);
	}
}
