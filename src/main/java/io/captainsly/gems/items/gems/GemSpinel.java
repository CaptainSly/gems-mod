package io.captainsly.gems.items.gems;

import io.captainsly.gems.GemsCreativeTab.GemsCreativeTabGem;
import io.captainsly.gems.lib.LibMisc;
import net.minecraft.item.Item;

public class GemSpinel extends Item {

	public GemSpinel() {
		setUnlocalizedName("spinelGem");
		setTextureName(LibMisc.MOD_ID + ":spinelGem");
		setCreativeTab(GemsCreativeTabGem.instance);
	}
	
}
