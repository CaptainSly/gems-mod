package io.captainsly.gems.items.gems;

import io.captainsly.gems.GemsCreativeTab.GemsCreativeTabGem;
import io.captainsly.gems.lib.LibMisc;
import net.minecraft.item.Item;

public class GemTopaz extends Item {

	public GemTopaz() {
		setUnlocalizedName("topazGem");
		setTextureName(LibMisc.MOD_ID + ":topazGem");
		setCreativeTab(GemsCreativeTabGem.instance);
	}
	
}
