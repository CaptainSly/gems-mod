package io.captainsly.gems.items.gems;

import io.captainsly.gems.GemsCreativeTab.GemsCreativeTabGem;
import io.captainsly.gems.lib.LibMisc;
import net.minecraft.item.Item;

public class GemSapphire extends Item {

	public GemSapphire() {
		setUnlocalizedName("sapphireGem");
		setTextureName(LibMisc.MOD_ID + ":sapphireGem");
		setCreativeTab(GemsCreativeTabGem.instance);
	}
	
}
