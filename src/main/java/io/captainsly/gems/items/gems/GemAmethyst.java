package io.captainsly.gems.items.gems;

import io.captainsly.gems.GemsCreativeTab.GemsCreativeTabGem;
import io.captainsly.gems.lib.LibMisc;
import net.minecraft.item.Item;

public class GemAmethyst extends Item {

	public GemAmethyst() {
		setUnlocalizedName("amethystGem");
		setTextureName(LibMisc.MOD_ID + ":amethystGem");
		setCreativeTab(GemsCreativeTabGem.instance);
	}
	
}
