package io.captainsly.gems.items.dusts;

import io.captainsly.gems.GemsCreativeTab.GemsCreativeTabDusts;
import io.captainsly.gems.lib.LibMisc;
import net.minecraft.item.Item;

public class DustSapphire extends Item {

	public DustSapphire() {
		setUnlocalizedName("sapphireDust");
		setTextureName(LibMisc.MOD_ID + ":sapphireDust");
		setCreativeTab(GemsCreativeTabDusts.instance);
	}
	
}
