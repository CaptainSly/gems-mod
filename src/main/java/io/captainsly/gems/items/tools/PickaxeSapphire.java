package io.captainsly.gems.items.tools;

import io.captainsly.gems.GemsCreativeTab.GemsCreativeTabTools;
import io.captainsly.gems.lib.LibMaterials;
import io.captainsly.gems.lib.LibMisc;
import net.minecraft.item.ItemPickaxe;

public class PickaxeSapphire extends ItemPickaxe {

	public PickaxeSapphire() {
		super(LibMaterials.SAPPHIRE_TOOL);
		setUnlocalizedName("sapphirePick");
		setTextureName(LibMisc.MOD_ID + ":sapphirePickaxe");
		setMaxStackSize(1);
		setCreativeTab(GemsCreativeTabTools.instance);
	}

}
