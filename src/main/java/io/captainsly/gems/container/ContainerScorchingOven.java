package io.captainsly.gems.container;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import io.captainsly.gems.handler.ScorchingFurnaceRecipes;
import io.captainsly.gems.tileentity.TileEntityScorchingOven;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.inventory.SlotFurnace;
import net.minecraft.item.ItemStack;

public class ContainerScorchingOven extends Container {

	private TileEntityScorchingOven scorchingOven;

	public int lastBurnTime;
	public int lastCurrentItemBurnTime;
	public int lastCookTime;

	public ContainerScorchingOven(InventoryPlayer inventory, TileEntityScorchingOven entity) {
		this.scorchingOven = entity;

		this.addSlotToContainer(new Slot(entity, 0, 56, 35));
		this.addSlotToContainer(new Slot(entity, 1, 8, 62));
		this.addSlotToContainer(new SlotFurnace(inventory.player, entity, 2, 116, 35));

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 9; j++) {
				this.addSlotToContainer(new Slot(inventory, j + i * 9 + 9, 8 + j * 18, 84 + i * 18));
			}
		}

		for (int i = 0; i < 9; i++) {
			this.addSlotToContainer(new Slot(inventory, i, 8 + i * 18, 142));
		}
	}

	public void addCraftingToCrafters(ICrafting crafting) {
		super.addCraftingToCrafters(crafting);
		crafting.sendProgressBarUpdate(this, 0, this.scorchingOven.cookTime);
		crafting.sendProgressBarUpdate(this, 1, this.scorchingOven.burnTime);
		crafting.sendProgressBarUpdate(this, 2, this.scorchingOven.currentItemBurnTime);
	}

	public void detectAndSendChanges() {
		super.detectAndSendChanges();

		for (int i = 0; i < this.crafters.size(); i++) {

			ICrafting craft = (ICrafting) this.crafters.get(i);

			if (this.lastCookTime != this.scorchingOven.cookTime) craft.sendProgressBarUpdate(this, 0, this.scorchingOven.cookTime);
			if (this.lastBurnTime != this.scorchingOven.burnTime) craft.sendProgressBarUpdate(this, 1, this.scorchingOven.burnTime);
			if (this.lastCurrentItemBurnTime != this.scorchingOven.currentItemBurnTime) craft.sendProgressBarUpdate(this, 2, this.scorchingOven.currentItemBurnTime);

		}

		this.lastCookTime = this.scorchingOven.cookTime;
		this.lastBurnTime = this.scorchingOven.burnTime;
		this.lastCurrentItemBurnTime = this.scorchingOven.currentItemBurnTime;
	}

	@SideOnly(Side.CLIENT)
	public void updateProgressBar(int i, int j) {
		if (i == 0) this.scorchingOven.cookTime = j;
		if (i == 1) this.scorchingOven.burnTime = j;
		if (i == 2) this.scorchingOven.currentItemBurnTime = j;
	}

	public ItemStack transferStackInSlot(EntityPlayer player, int i) {
		ItemStack itemstack = null;
		Slot slot = (Slot) this.inventorySlots.get(i);

		if (slot != null && slot.getHasStack()) {
			ItemStack itemstack1 = slot.getStack();
			itemstack = itemstack1.copy();

			if (i == 2) {
				if (!this.mergeItemStack(itemstack1, 3, 39, true)) { return null; }

				slot.onSlotChange(itemstack1, itemstack);
			} else if (i != 1 && i != 0) {
				if (ScorchingFurnaceRecipes.smelting().getSmeltingResult(itemstack1) != null) {
					if (!this.mergeItemStack(itemstack1, 0, 1, false)) { return null; }
				} else if (TileEntityScorchingOven.isItemFuel(itemstack1)) {
					if (!this.mergeItemStack(itemstack1, 1, 2, false)) { return null; }
				} else if (i >= 3 && i < 30) {
					if (!this.mergeItemStack(itemstack1, 30, 39, false)) { return null; }
				} else if (i >= 30 && i < 39 && !this.mergeItemStack(itemstack1, 3, 30, false)) { return null; }
			} else if (!this.mergeItemStack(itemstack1, 3, 39, false)) { return null; }

			if (itemstack1.stackSize == 0) {
				slot.putStack((ItemStack) null);
			} else {
				slot.onSlotChanged();
			}

			if (itemstack1.stackSize == itemstack.stackSize) { return null; }

			slot.onPickupFromSlot(player, itemstack1);
		}

		return itemstack;
	}

	@Override
	public boolean canInteractWith(EntityPlayer player) {
		return true;
	}

}
