package io.captainsly.gems.init;

import io.captainsly.gems.handler.ScorchingFurnaceRecipes;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

/**
 * A way to add to any custom recipes that have been made
 * for the following blocks
 * {@link BlockScorchingOven}, {@link BlockFusingOven}
 * 
 * @author Zachary Knoblauch
 */
public final class GemsRegistry {

	
	/*
	===============================
	|						      |
	|		 SMELTING BLOCK	 	  |
	|							  |
	===============================
	*/
	
	/**
	 * <p>
	 * Adds a recipe to the Scorched Oven
	 * 
	 * {@link BlockScorchingOven}
	 * </p>
	 * Uses the {@link ScorchingFuranceRecipes} class to register recipes
	 * 
	 * @param input -> The Block you wish to smelt
	 * @param output -> The resulting item/block that comes out of the smelting block/item
	 * @param xp -> A float value of exp
	 */
	public static void addScorchingSmeltingRecipe(Block input, ItemStack output, float xp) {
		ScorchingFurnaceRecipes.smelting().addRecipe(input, output, xp);
	}

	/**
	 * <p>
	 * Adds a recipe to the Scorched Oven
	 * 
	 * {@link BlockScorchingOven}
	 * </p>
	 * Uses the {@link ScorchingFuranceRecipes} class to register recipes
	 * 
	 * @param input -> The Item you wish to smelt
	 * @param output -> The resulting item/block that comes out of the smelting block/item
	 * @param xp -> A float value of exp
	 */
	public static void addScorchingSmeltingRecipe(Item input, ItemStack output, float xp) {
		ScorchingFurnaceRecipes.smelting().addRecipe(input, output, xp);
	}

	/**
	 * <p>
	 * Adds a recipe to the Scorched Oven
	 * 
	 * {@link BlockScorchingOven}
	 * </p>
	 * Uses the {@link ScorchingFuranceRecipes} class to register recipes
	 * 
	 * @param input -> The ItemStack you wish to smelt
	 * @param output -> The resulting item/block that comes out of the smelting block/item
	 * @param xp -> A float value of exp
	 */
	public static void addScorchingSmeltingRecipe(ItemStack input, ItemStack output, float xp) {
		ScorchingFurnaceRecipes.smelting().addRecipe(input, output, xp);
	}

}
