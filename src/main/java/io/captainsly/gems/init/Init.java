package io.captainsly.gems.init;

import cpw.mods.fml.common.registry.GameRegistry;
import io.captainsly.gems.GemsCreativeTab.GemsCreativeTabMachines;
import io.captainsly.gems.blocks.machines.FusionFurnace;
import io.captainsly.gems.blocks.machines.ScorchingOven;
import io.captainsly.gems.blocks.ores.OreAmethyst;
import io.captainsly.gems.blocks.ores.OreRuby;
import io.captainsly.gems.blocks.ores.OreSapphire;
import io.captainsly.gems.blocks.ores.OreSpinel;
import io.captainsly.gems.blocks.ores.OreTopaz;
import io.captainsly.gems.items.dusts.DustSapphire;
import io.captainsly.gems.items.gems.GemAmethyst;
import io.captainsly.gems.items.gems.GemRuby;
import io.captainsly.gems.items.gems.GemSapphire;
import io.captainsly.gems.items.gems.GemSpinel;
import io.captainsly.gems.items.gems.GemTopaz;
import io.captainsly.gems.items.tools.PickaxeSapphire;
import io.captainsly.gems.tileentity.TileEntityFusingOven;
import io.captainsly.gems.tileentity.TileEntityScorchingOven;
import net.minecraft.block.Block;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

public class Init {

	public static final class ModItems {
		
		public static Item sapphireGem;
		public static Item rubyGem;
		public static Item spinelGem;
		public static Item topazGem;
		public static Item amethystGem;
		
		public static Item sapphireDust;
		
		public static Item sapphirePick;
		
		public static final void init() {
			// Initialize Variables here
			sapphireGem = new GemSapphire();
			rubyGem = new GemRuby();
			spinelGem = new GemSpinel();
			topazGem = new GemTopaz();
			amethystGem = new GemAmethyst();
			
			sapphireDust = new DustSapphire();
			
			sapphirePick = new PickaxeSapphire();	
			
			// Register Items here
			GameRegistry.registerItem(sapphireGem, sapphireGem.getUnlocalizedName().substring(5));
			GameRegistry.registerItem(rubyGem, rubyGem.getUnlocalizedName().substring(5));
			GameRegistry.registerItem(spinelGem, spinelGem.getUnlocalizedName().substring(5));
			GameRegistry.registerItem(topazGem, topazGem.getUnlocalizedName().substring(5));
			GameRegistry.registerItem(amethystGem, amethystGem.getUnlocalizedName().substring(5));
			
			GameRegistry.registerItem(sapphireDust, sapphireDust.getUnlocalizedName().substring(5));
			
			GameRegistry.registerItem(sapphirePick, sapphirePick.getUnlocalizedName().substring(5));
		}
		
	}
	
	public static final class ModCrafting {
		
		public static final void init() {
			GameRegistry.addRecipe(new ItemStack(ModItems.sapphirePick), "###", " s ", " s ", '#', ModItems.sapphireGem, 's', Items.stick);
			
			GemsRegistry.addScorchingSmeltingRecipe(new ItemStack(ModItems.sapphireGem), new ItemStack(ModItems.sapphireDust), 0.285F);
		}
	}

	public static final class ModEntities {
		
		public static final void init() {
			
			GameRegistry.registerTileEntity(TileEntityScorchingOven.class, "scorchingOvenEntity");
			GameRegistry.registerTileEntity(TileEntityFusingOven.class, "fusingOvenEntity");
		}
	}
	
	public static final class ModBlocks {
		
		public static Block sapphireOre;
		public static Block rubyOre;
		public static Block spinelOre;
		public static Block topazOre;
		public static Block amethystOre;
		
		public static Block blockScorchingOvenIdle;
		public static Block blockScorchingOvenActive;
		
		public static Block blockFusionFurnaceIdle;
		public static Block blockFusionFurnaceActive;
	
		public static final void init() {
			// Initialize Variables here
			sapphireOre = new OreSapphire();
			rubyOre = new OreRuby();
			spinelOre = new OreSpinel();
			topazOre = new OreTopaz();
			amethystOre = new OreAmethyst();
			
			blockScorchingOvenIdle = new ScorchingOven(false).setBlockName("scorchingOvenIdle").setCreativeTab(GemsCreativeTabMachines.instance);
			blockScorchingOvenActive = new ScorchingOven(true).setBlockName("scorchingOvenActive").setLightLevel(0.625F);
			
			blockFusionFurnaceIdle = new FusionFurnace(false).setBlockName("fusionFurnaceIdle").setCreativeTab(GemsCreativeTabMachines.instance);
			blockFusionFurnaceActive = new FusionFurnace(true).setBlockName("fusionFurnaceActive").setLightLevel(0.625F);
			
			// Register Blocks here
			GameRegistry.registerBlock(sapphireOre, sapphireOre.getUnlocalizedName().substring(5));
			GameRegistry.registerBlock(rubyOre, rubyOre.getUnlocalizedName().substring(5));
			GameRegistry.registerBlock(spinelOre, spinelOre.getUnlocalizedName().substring(5));
			GameRegistry.registerBlock(topazOre, topazOre.getUnlocalizedName().substring(5));
			GameRegistry.registerBlock(amethystOre, amethystOre.getUnlocalizedName().substring(5));
			
			GameRegistry.registerBlock(blockScorchingOvenIdle, blockScorchingOvenIdle.getUnlocalizedName().substring(5));
			GameRegistry.registerBlock(blockScorchingOvenActive, blockScorchingOvenActive.getUnlocalizedName().substring(5));
			
			GameRegistry.registerBlock(blockFusionFurnaceIdle, blockFusionFurnaceIdle.getUnlocalizedName().substring(5));
			GameRegistry.registerBlock(blockFusionFurnaceActive, blockFusionFurnaceActive.getUnlocalizedName().substring(5));
		}
	}

}