package io.captainsly.gems.tileentity;

import cpw.mods.fml.common.registry.GameRegistry;
import io.captainsly.gems.blocks.machines.ScorchingOven;
import io.captainsly.gems.handler.ScorchingFurnaceRecipes;
import net.minecraft.block.Block;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.inventory.ISidedInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;

public class TileEntityScorchingOven extends TileEntity implements ISidedInventory {

	private String localizedName;

	private static final int[] slots_top = new int[] { 0 };
	private static final int[] slots_bottom = new int[] { 2, 1 };
	private static final int[] slots_side = new int[] { 1 };
	
	private ItemStack[] slots = new ItemStack[3];
	
	public int furnaceSpeed = 150;
	public int burnTime;
	public int currentItemBurnTime;
	public int cookTime;
	
	public void setGuiDisplayName(String displayName) {
		this.localizedName = displayName;
	}
	
	public String getInventoryName() {
		return this.hasCustomInventoryName() ? this.localizedName : "container.scorchingOven";
	}

	public boolean hasCustomInventoryName() {
		return this.localizedName != null && this.localizedName.length() > 0;
	}
	
	public int getSizeInventory() {
		return this.slots.length;
	}
	
	@Override
	public ItemStack getStackInSlot(int i) {
		return this.slots[i];
	}
	
	@Override
	public ItemStack decrStackSize(int i, int j) {
		if (this.slots[i] != null) {
			ItemStack stack;
			
			if (this.slots[i].stackSize <= j) {
				stack = this.slots[i];
				this.slots[i] = null;
				return stack;
			} else {
				stack = this.slots[i].splitStack(j);
				
				if (this.slots[i].stackSize == 0) this.slots[i] = null;
				
				return stack;
			}
		} else
			return null;
	}
	
	@Override
	public ItemStack getStackInSlotOnClosing(int i) {
		if (this.slots[i] != null) {
			ItemStack stack = this.slots[i];
			this.slots[i] = null;
			return stack;
		}
		
		return null;
	}
	
	@Override
	public void setInventorySlotContents(int i, ItemStack stack) {
		this.slots[i] = stack;
		
		if (stack != null && stack.stackSize > this.getInventoryStackLimit()) stack.stackSize = this.getInventoryStackLimit();
	}
	
	@Override
	public int getInventoryStackLimit() {
		return 64;
	}
	
	@Override
	public boolean isUseableByPlayer(EntityPlayer player) {
		return this.worldObj.getTileEntity(this.xCoord, this.yCoord, this.zCoord) != this ? false : player.getDistanceSq((double)this.xCoord + 0.5D, (double)this.yCoord + 0.5D,(double)this.zCoord + 0.5D) <= 64.0D;
	}
	
	public void openInventory() {}
	public void closeInventory() {}
	
	@Override
	public boolean isItemValidForSlot(int i, ItemStack stack) {
		return i == 2 ? false : (i == 1 ? isItemFuel(stack) : true);
	}
	
	public static boolean isItemFuel(ItemStack stack) {
		return getItemBurnTime(stack) > 0;
	}
	
	public static int getItemBurnTime(ItemStack stack) {
		if (stack == null) return 0;
		else {
			Item item = stack.getItem();
			
			if (item instanceof ItemBlock && Block.getBlockFromItem(item) != Blocks.air) {
				Block block = Block.getBlockFromItem(item);
				
				if (block == Blocks.obsidian) return 3600;
			}
			
			if (item == Items.lava_bucket) return 1600;
			if (item == Items.blaze_rod) return 800;
			if (item == Items.blaze_powder) return 400;
			if (item == Items.coal) return 100;
		}
		
		return GameRegistry.getFuelValue(stack);
	}
	
	public boolean isBurning() {
		return this.burnTime > 0;
	}
	
	public void updateEntity() {
		boolean flag = this.burnTime > 0;
		boolean flag1 = false;
		
		if (this.isBurning()) this.burnTime--;
		
		if (!this.worldObj.isRemote) {
			if (this.burnTime == 0 && this.canSmelt()) {
				this.currentItemBurnTime = this.burnTime = getItemBurnTime(this.slots[1]);
				
				if (this.isBurning()) {
					flag1 = true;
					
					if (this.slots[1] != null) {
						this.slots[1].stackSize--;
						
						if (this.slots[1].stackSize == 0) {
							this.slots[1] = this.slots[1].getItem().getContainerItem(this.slots[1]);
						}
					}
				}
			}
			
			if (this.isBurning() && this.canSmelt()) {
				this.cookTime++;
				
				if (this.cookTime == this.furnaceSpeed) {
					this.cookTime = 0;
					this.smeltItem();
					flag1 = true;
				}
			} else
				this.cookTime = 0;
			
			if (flag != this.isBurning()) {
				flag1 = true;
				ScorchingOven.updateScorchingOvenBlockState(this.burnTime > 0, this.worldObj, this.xCoord, this.yCoord, this.zCoord);
			}
		}
		
		if (flag1) this.markDirty();
	}
	
	public boolean canSmelt() {
		if (this.slots[0] == null) return false;
		else {
			ItemStack stack = ScorchingFurnaceRecipes.smelting().getSmeltingResult(this.slots[0]);
			
			if (stack == null) return false;
			if (this.slots[2] == null) return true;
			if (!this.slots[2].isItemEqual(stack)) return false;
			
			int result = this.slots[2].stackSize + stack.stackSize;
			
			return (result <= getInventoryStackLimit() && result <= stack.getMaxStackSize());
		}
	}
	
	public void smeltItem() {
		if (this.canSmelt()) {
			ItemStack stack = ScorchingFurnaceRecipes.smelting().getSmeltingResult(this.slots[0]);
			
			if (this.slots[2] == null) this.slots[2] = stack.copy();
			else if (this.slots[2].isItemEqual(stack)) this.slots[2].stackSize += stack.stackSize;
			
			this.slots[0].stackSize--;
			
			if (this.slots[0].stackSize <= 0) this.slots[0] = null;
		}
	}
	
	@Override
	public int[] getAccessibleSlotsFromSide(int i) {
		return i == 0 ? slots_bottom : (i == 1 ? slots_top : slots_side);
	}
	
	@Override
	public boolean canInsertItem(int i, ItemStack stack, int j) {
		return this.isItemValidForSlot(i, stack);
	}
	
	@Override
	public boolean canExtractItem(int i, ItemStack stack, int j) {
		return j != 0 || i != 1 || stack.getItem() == Items.bucket;
	}
	
	public int getBurnTimeRemainingScaled(int i) {
		if (this.currentItemBurnTime == 0) this.currentItemBurnTime = this.furnaceSpeed;
		
		return this.burnTime * i / this.currentItemBurnTime;
	}
	
	public int getCookProgressScaled(int i) {
		return this.cookTime * i / this.furnaceSpeed;
	}
	
}
