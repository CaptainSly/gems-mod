package io.captainsly.gems;

import io.captainsly.gems.init.Init.ModBlocks;
import io.captainsly.gems.init.Init.ModItems;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;

public abstract class GemsCreativeTab extends CreativeTabs {
	
	protected Item tabIcon;
	private boolean searchBar;
	
	public GemsCreativeTab(String lable) {
		super(lable);
	}

	@Override
	public boolean hasSearchBar() {
		return searchBar;
	}
	
	public void setHasSearchBar(boolean search) {
		searchBar = search;
	}
	
	@Override
	public Item getTabIconItem() {
		return Item.getItemFromBlock(Blocks.cactus);
	}
	
	public static class GemsCreativeTabGem extends GemsCreativeTab {

		public static final GemsCreativeTabGem instance = new GemsCreativeTabGem();
		
		public GemsCreativeTabGem() {
			super("gemsTabGem");
		}
		
		@Override
		public Item getTabIconItem() {
			return ModItems.sapphireGem;
		}
	}

	public static class GemsCreativeTabOre extends GemsCreativeTab {
		
		public static final GemsCreativeTabOre instance = new GemsCreativeTabOre();
		
		public GemsCreativeTabOre() {
			super("gemsTabOre");
		}
		
		@Override
		public Item getTabIconItem() {
			return Item.getItemFromBlock(ModBlocks.sapphireOre);
		}
	}
	
	public static class GemsCreativeTabTools extends GemsCreativeTab {
		
		public static final GemsCreativeTabTools instance = new GemsCreativeTabTools();
		
		public GemsCreativeTabTools() {
			super("gemsTabTools");
		}
		
		@Override
		public Item getTabIconItem() {
			return ModItems.sapphirePick;
		}
	}
	
	public static class GemsCreativeTabMetals extends GemsCreativeTab {
		
		public static final GemsCreativeTabMetals instance = new GemsCreativeTabMetals();
		
		public GemsCreativeTabMetals() {
			super("gemsTabMetals");
		}
		
		//TODO: Add a metal item and switch it here
		@Override
		public Item getTabIconItem() {
			return ModItems.spinelGem;
		}
	}
	
	public static class GemsCreativeTabMachines extends GemsCreativeTab {
		
		public static final GemsCreativeTabMachines instance = new GemsCreativeTabMachines();
		
		public GemsCreativeTabMachines() {
			super("gemsTabMachines");
		}
		
		@Override
		public Item getTabIconItem() {
			return Item.getItemFromBlock(ModBlocks.blockScorchingOvenIdle);
		}
	}
	
	public static class GemsCreativeTabDusts extends GemsCreativeTab {
		
		public static final GemsCreativeTab instance = new GemsCreativeTabDusts();
		
		public GemsCreativeTabDusts() {
			super("gemsTabDusts");
		}
		
		@Override
		public Item getTabIconItem() {
			return ModItems.sapphireDust;
		}
	}
	
}
