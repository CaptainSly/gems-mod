package io.captainsly.gems.handler;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;

@SuppressWarnings("rawtypes")
public class ScorchingFurnaceRecipes {

	private static final ScorchingFurnaceRecipes smeltingBase = new ScorchingFurnaceRecipes();
	
	private Map smeltingList = new HashMap();
	private Map experienceList = new HashMap();
	
	public static ScorchingFurnaceRecipes smelting() {
		return smeltingBase;
	}
	
	private ScorchingFurnaceRecipes() {
		
	}
	
	public void addRecipe(Block block, ItemStack stack, float exp) {
		this.addRecipe(Item.getItemFromBlock(block), stack, exp);
	}
	
	public void addRecipe(Item item, ItemStack stack, float exp) {
		this.addRecipe(new ItemStack(item, 1, 32767), stack, exp);
	}
	
	@SuppressWarnings("unchecked")
	public void addRecipe(ItemStack input, ItemStack output, float exp) {
		this.smeltingList.put(input, output);
		this.experienceList.put(output, Float.valueOf(exp));
	}
	
	public ItemStack getSmeltingResult(ItemStack stack) {
		Iterator iterator = this.smeltingList.entrySet().iterator();
		Entry entry;
		
		do {
			if (!iterator.hasNext()) 
				return null;
			
			entry = (Entry) iterator.next();
		}
		while (!this.func(stack, (ItemStack) entry.getKey()));
		
		return (ItemStack) entry.getValue();
	}
	
	private boolean func(ItemStack input, ItemStack output) {
		return output.getItem() == input.getItem() && (output.getItemDamage() == 32767 || output.getItemDamage() == input.getItemDamage());
	}
	
	public Map getSmeltingList() {
		return this.smeltingList;
	}
	
	public float func_b(ItemStack stack) {
		float ret = stack.getItem().getSmeltingExperience(stack);
		if (ret != -1) return ret;
		
		Iterator iterator = this.experienceList.entrySet().iterator();
		Entry entry;
		
		do {
			if (!iterator.hasNext())
				return 0.0F;
			
			entry = (Entry)iterator.next();
		}
		while (!this.func(stack, (ItemStack) entry.getKey()));
		
		return ((Float) entry.getValue()).floatValue();
	}
	
}
