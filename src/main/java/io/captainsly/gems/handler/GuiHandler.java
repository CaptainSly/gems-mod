package io.captainsly.gems.handler;

import cpw.mods.fml.common.network.IGuiHandler;
import io.captainsly.gems.container.ContainerFusionFurnace;
import io.captainsly.gems.container.ContainerScorchingOven;
import io.captainsly.gems.gui.GuiScorchingOven;
import io.captainsly.gems.lib.LibGuiId;
import io.captainsly.gems.tileentity.TileEntityFusingOven;
import io.captainsly.gems.tileentity.TileEntityFusionFurnace;
import io.captainsly.gems.tileentity.TileEntityScorchingOven;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class GuiHandler implements IGuiHandler{

	@Override
	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		TileEntity entity = world.getTileEntity(x, y, z);
		
		if (entity != null) {
			switch(ID) {
				case LibGuiId.SCORCHING_OVEN:
					if (entity instanceof TileEntityScorchingOven)
						return new ContainerScorchingOven(player.inventory, (TileEntityScorchingOven) entity);
					
					return null;
				case LibGuiId.FUSING_FURNACE:
					if (entity instanceof TileEntityFusingOven) {
						return new ContainerFusionFurnace(player.inventory, (TileEntityFusionFurnace) entity);
					}
					return null;
			}
				
		}
		
		return null;
	}

	@Override
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		TileEntity entity = world.getTileEntity(x, y, z);
		
		if (entity != null) {
			switch(ID) {
				case LibGuiId.SCORCHING_OVEN:
					if (entity instanceof TileEntityScorchingOven)
						return new GuiScorchingOven(player.inventory, (TileEntityScorchingOven) entity);
					
					return null;
				case LibGuiId.FUSING_FURNACE:
					if (entity instanceof TileEntityFusingOven) {
					}
					return null;
			}
				
		}
		
		return null;
	}

}
