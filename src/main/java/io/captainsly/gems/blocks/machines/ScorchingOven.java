package io.captainsly.gems.blocks.machines;

import java.util.Random;

import cpw.mods.fml.common.network.internal.FMLNetworkHandler;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import io.captainsly.gems.Gems;
import io.captainsly.gems.init.Init.ModBlocks;
import io.captainsly.gems.lib.LibGuiId;
import io.captainsly.gems.lib.LibMisc;
import io.captainsly.gems.tileentity.TileEntityScorchingOven;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

public class ScorchingOven extends BlockContainer {

	private final boolean isActive;

	@SideOnly(Side.CLIENT)
	private IIcon iconFront;

	@SideOnly(Side.CLIENT)
	private IIcon iconTop;

	private static boolean keepInventory;
	private Random rand = new Random();

	public ScorchingOven(boolean isBurning) {
		super(Material.iron);
		this.isActive = isBurning;
	}

	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister icon) {
		this.blockIcon = icon.registerIcon(LibMisc.MOD_ID + ":scorchingOvenSide");
		this.iconFront = icon.registerIcon(LibMisc.MOD_ID + ":" + (this.isActive ? "scorchingOvenFrontOn" : "scorchingOvenFrontOff"));
		this.iconTop = icon.registerIcon(LibMisc.MOD_ID + ":scorchingOvenTop");
	}

	@SideOnly(Side.CLIENT)
	public IIcon getIcon(int side, int metadata) {
		return metadata == 0 && side == 3 ? this.iconFront : side == 1 ? this.iconTop : (side == 0 ? this.iconTop : (side == metadata ? this.iconFront : this.blockIcon));
	}

	public Item getItemDropped(int i, Random rand, int j) {
		return Item.getItemFromBlock(ModBlocks.blockScorchingOvenIdle);
	}

	public void onBlockAdded(World w, int x, int y, int z) {
		super.onBlockAdded(w, x, y, z);
		this.setDefaultDirection(w, x, y, z);
	}

	public void setDefaultDirection(World w, int x, int y, int z) {
		if (!w.isRemote) {
			Block b1 = w.getBlock(x, y, z - 1);
			Block b2 = w.getBlock(x, y, z + 1);
			Block b3 = w.getBlock(x - 1, y, z);
			Block b4 = w.getBlock(x + 1, y, z);

			byte b0 = 3;

			if (b1.func_149730_j() && !b2.func_149730_j()) b0 = 3;
			if (b2.func_149730_j() && !b1.func_149730_j()) b0 = 2;
			if (b3.func_149730_j() && !b4.func_149730_j()) b0 = 5;
			if (b4.func_149730_j() && !b3.func_149730_j()) b0 = 4;

			w.setBlockMetadataWithNotify(x, y, z, b0, 2);
		}
	}

	public boolean onBlockActivated(World w, int x, int y, int z, EntityPlayer player, int side, float offX, float offY, float offZ) {
		if (!w.isRemote) FMLNetworkHandler.openGui(player, Gems.instance, LibGuiId.SCORCHING_OVEN, w, x, y, z);
		return true;
	}

	@Override
	public TileEntity createNewTileEntity(World w, int i) {
		return new TileEntityScorchingOven();
	}

	@SideOnly(Side.CLIENT)
	public void randomDisplayTick(World w, int x, int y, int z, Random rand) {
		if (this.isActive) {
			int l = w.getBlockMetadata(x, y, z);
			float f = (float) x + 0.5F;
			float f1 = (float) y + 0.0F + rand.nextFloat() * 6.0F / 16.0F;
			float f2 = (float) z + 0.5F;
			float f3 = 0.52F;
			float f4 = rand.nextFloat() * 0.6F - 0.3F;

			if (l == 4) {
				w.spawnParticle("smoke", (double) (f - f3), (double) f1, (double) (f2 + f4), 0.0D, 0.0D, 0.0D);
				w.spawnParticle("flame", (double) (f - f3), (double) f1, (double) (f2 + f4), 0.0D, 0.0D, 0.0D);
			} else if (l == 5) {
				w.spawnParticle("smoke", (double) (f + f3), (double) f1, (double) (f2 + f4), 0.0D, 0.0D, 0.0D);
				w.spawnParticle("flame", (double) (f + f3), (double) f1, (double) (f2 + f4), 0.0D, 0.0D, 0.0D);
			} else if (l == 2) {
				w.spawnParticle("smoke", (double) (f + f4), (double) f1, (double) (f2 - f3), 0.0D, 0.0D, 0.0D);
				w.spawnParticle("flame", (double) (f + f4), (double) f1, (double) (f2 - f3), 0.0D, 0.0D, 0.0D);
			} else if (l == 3) {
				w.spawnParticle("smoke", (double) (f + f4), (double) f1, (double) (f2 + f3), 0.0D, 0.0D, 0.0D);
				w.spawnParticle("flame", (double) (f + f4), (double) f1, (double) (f2 + f3), 0.0D, 0.0D, 0.0D);
			}
		}
	}

	public void onBlockPlacedBy(World w, int x, int y, int z, EntityLivingBase player, ItemStack stack) {
		int l = MathHelper.floor_double((double) (player.rotationYaw * 4.0F / 360.0F) + 0.5D) & 3;

		if (l == 0) w.setBlockMetadataWithNotify(x, y, z, 2, 2);
		if (l == 1) w.setBlockMetadataWithNotify(x, y, z, 5, 2);
		if (l == 2) w.setBlockMetadataWithNotify(x, y, z, 3, 2);
		if (l == 3) w.setBlockMetadataWithNotify(x, y, z, 4, 2);
		if (stack.hasDisplayName()) ((TileEntityScorchingOven) w.getTileEntity(x, y, z)).setGuiDisplayName(stack.getDisplayName());
	}

	public static void updateScorchingOvenBlockState(boolean active, World worldObj, int xCoord, int yCoord, int zCoord) {
		int i = worldObj.getBlockMetadata(xCoord, yCoord, zCoord);

		TileEntity tile = worldObj.getTileEntity(xCoord, yCoord, zCoord);
		keepInventory = true;

		if (active) worldObj.setBlock(xCoord, yCoord, zCoord, ModBlocks.blockScorchingOvenActive);
		else worldObj.setBlock(xCoord, yCoord, zCoord, ModBlocks.blockScorchingOvenIdle);

		keepInventory = false;

		worldObj.setBlockMetadataWithNotify(xCoord, yCoord, zCoord, i, 2);

		if (tile != null) {
			tile.validate();
			worldObj.setTileEntity(xCoord, yCoord, zCoord, tile);
		}
	}

	public void breakBlock(World w, int x, int y, int z, Block oldBlock, int oldMetadata) {
		if (!keepInventory) {
			TileEntityScorchingOven tile = (TileEntityScorchingOven) w.getTileEntity(x, y, z);
			
			if (tile != null) {
				for (int i = 0; i < tile.getSizeInventory(); i++) {
					ItemStack stack = tile.getStackInSlot(i);
					
					if (stack != null) {
						float f = this.rand.nextFloat() * 0.8F + 0.1F;
						float f1 = this.rand.nextFloat() * 0.8F + 0.1F;
						float f2 = this.rand.nextFloat() * 0.8F + 0.1F;
						
						while (stack.stackSize > 0) {
							int j = this.rand.nextInt(21) + 10;
							
							if (j > stack.stackSize)
								j = stack.stackSize;
							
							stack.stackSize -= j;
							
							EntityItem item = new EntityItem(w, (double)((float)x + f), (double)((float)y + f1), (double)((float)z + f2), new ItemStack(stack.getItem(), j, stack.getItemDamage()));
							
							if (stack.hasTagCompound()) item.getEntityItem().setTagCompound((NBTTagCompound)stack.getTagCompound().copy());
							
							w.spawnEntityInWorld(item);
						}
					}
				}
				w.func_147453_f(x, y, z, oldBlock);
			}
		}
		super.breakBlock(w, x, y, z, oldBlock, oldMetadata);
	}
	
	public Item getItem(World world, int x, int y, int z) {
		return Item.getItemFromBlock(ModBlocks.blockScorchingOvenIdle);
	}

}
