package io.captainsly.gems.blocks.machines;

import java.util.Random;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import io.captainsly.gems.lib.LibMisc;
import net.minecraft.block.Block;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.world.World;

public class FusionFurnace extends BlockContainer {

	private Random rand;
	private final boolean isActive;
	private static boolean keepInventory = false;
	
	@SideOnly(Side.CLIENT)
	private IIcon iconFront;
	
	public FusionFurnace(boolean isActive) {
		super(Material.iron);
		setHardness(3.0F);
		rand = new Random();
		
		this.isActive = isActive;
	}
	
	@SideOnly(Side.CLIENT)
	public void registerBlockIcons(IIconRegister icon) {
		this.blockIcon = icon.registerIcon(LibMisc.MOD_ID + ":" + (this.isActive ? "fusionFurnaceSideOn" : "fusionFurnaceSideOff"));
		this.iconFront = icon.registerIcon(LibMisc.MOD_ID + ":" + (this.isActive ? "fusionFurnaceFrontOn" : "fusionFrontOff"));
	}

	@SideOnly(Side.CLIENT)
	public IIcon getIcon(int side, int metadata) {
		return metadata == 0 && side == 3 ? this.iconFront : (side == metadata ? this.iconFront : this.blockIcon);
	}
	
	@SideOnly(Side.CLIENT)
	public void onBlockAdded(World w, int x, int y, int z) {
		super.onBlockAdded(w, x, y, z);
		this.setDefaultDirection(w, x, y, z);
	}
	
	public void setDefaultDirection(World w, int x, int y, int z) {
		if (!w.isRemote) {
			Block b1 = w.getBlock(x, y, z - 1);
			Block b2 = w.getBlock(x, y, z + 1);
			Block b3 = w.getBlock(x - 1, y, z);
			Block b4 = w.getBlock(x + 1, y, z);

			byte b0 = 3;

			if (b1.func_149730_j() && !b2.func_149730_j()) b0 = 3;
			if (b2.func_149730_j() && !b1.func_149730_j()) b0 = 2;
			if (b3.func_149730_j() && !b4.func_149730_j()) b0 = 5;
			if (b4.func_149730_j() && !b3.func_149730_j()) b0 = 4;

			w.setBlockMetadataWithNotify(x, y, z, b0, 2);
		}
	}
	
	@Override
	public TileEntity createNewTileEntity(World p_149915_1_, int p_149915_2_) {
		return null;
	}

}
