package io.captainsly.gems.blocks.ores;

import java.util.Random;

import io.captainsly.gems.GemsCreativeTab.GemsCreativeTabOre;
import io.captainsly.gems.init.Init.ModItems;
import io.captainsly.gems.lib.LibMisc;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class OreRuby extends Block {

	public OreRuby() {
		super(Material.rock);
		setBlockName("rubyOre");
		setBlockTextureName(LibMisc.MOD_ID + ":rubyOre");
		setHardness(2.0F);
		setHarvestLevel("pickaxe", 2);
		setCreativeTab(GemsCreativeTabOre.instance);
	}
	
	public Item getItemDropped(int i, Random rand, int j) {
		return ModItems.rubyGem;
	}
	
	public int quantityDropped(Random rand) {
		return 1;
	}
	
}
