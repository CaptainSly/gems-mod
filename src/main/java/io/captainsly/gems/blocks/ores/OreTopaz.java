package io.captainsly.gems.blocks.ores;

import java.util.Random;

import io.captainsly.gems.GemsCreativeTab.GemsCreativeTabOre;
import io.captainsly.gems.init.Init.ModItems;
import io.captainsly.gems.lib.LibMisc;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class OreTopaz extends Block {

	public OreTopaz() {
		super(Material.rock);
		setBlockName("topazOre");
		setBlockTextureName(LibMisc.MOD_ID + ":topazOre");
		setHardness(2.0F);
		setHarvestLevel("pickaxe", 2);
		setCreativeTab(GemsCreativeTabOre.instance);
	}
	
	public Item getItemDropped(int i, Random rand, int j) {
		return ModItems.topazGem;
	}
	
	public int quantityDropped(Random random) {
		return 1 + random.nextInt(2);
	}
	
}
