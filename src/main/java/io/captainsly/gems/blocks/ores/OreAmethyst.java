package io.captainsly.gems.blocks.ores;

import java.util.Random;

import io.captainsly.gems.GemsCreativeTab.GemsCreativeTabOre;
import io.captainsly.gems.init.Init.ModItems;
import io.captainsly.gems.lib.LibMisc;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class OreAmethyst extends Block {

	public OreAmethyst() {
		super(Material.rock);
		setBlockName("amethystOre");
		setBlockTextureName(LibMisc.MOD_ID + ":amethystOre");
		setHardness(2.0F);
		setHarvestLevel("pickaxe", 2);
		setCreativeTab(GemsCreativeTabOre.instance);
	}

	public Item getItemDropped(int i, Random rand, int j) {
		return ModItems.amethystGem;
	}
	
	public int quantityDropped(Random rand) {
		return 1;
	}
	
}
