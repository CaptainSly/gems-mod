package io.captainsly.gems.blocks.ores;

import java.util.Random;

import io.captainsly.gems.GemsCreativeTab.GemsCreativeTabOre;
import io.captainsly.gems.init.Init.ModItems;
import io.captainsly.gems.lib.LibMisc;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;

public class OreSpinel extends Block {

	public OreSpinel() {
		super(Material.rock);
		setBlockName("spinelOre");
		setBlockTextureName(LibMisc.MOD_ID + ":spinelOre");
		setCreativeTab(GemsCreativeTabOre.instance);
	}
	
	public Item getItemDropped(int i, Random rand, int j) {
		return ModItems.spinelGem;
	}
	
	public int quantityDropped(Random rand) {
		return 2 + rand.nextInt(3);
	}

}
