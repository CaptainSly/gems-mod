package io.captainsly.gems.gui;

import org.lwjgl.opengl.GL11;

import io.captainsly.gems.container.ContainerScorchingOven;
import io.captainsly.gems.lib.LibRes;
import io.captainsly.gems.tileentity.TileEntityScorchingOven;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.InventoryPlayer;

public class GuiScorchingOven extends GuiContainer {

	public TileEntityScorchingOven scorchingOven;

	public GuiScorchingOven(InventoryPlayer inventoryPlayer, TileEntityScorchingOven entity) {
		super(new ContainerScorchingOven(inventoryPlayer, entity));

		this.scorchingOven = entity;

		this.xSize = 176;
		this.ySize = 166;
	}

	public void drawGuiContainerForegroundLayer(int i, int j) {
		String name = this.scorchingOven.hasCustomInventoryName() ? this.scorchingOven.getInventoryName() : I18n.format(this.scorchingOven.getInventoryName(), new Object[0]);
		this.fontRendererObj.drawString(name, this.xSize / 2 - this.fontRendererObj.getStringWidth(name) / 2, 6, 4210752);
		this.fontRendererObj.drawString(I18n.format("container.inventory", new Object[0]), 100, this.ySize - 96 + 2, 4210752);
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(float a, int b, int c) {
		GL11.glColor4f(1F, 1F, 1F, 1F);
		Minecraft.getMinecraft().getTextureManager().bindTexture(LibRes.SCORCHING_OVEN_GUI);
		drawTexturedModalRect(guiLeft, guiTop, 0, 0, xSize, ySize);

		if (this.scorchingOven.isBurning()) {
			int k = this.scorchingOven.getBurnTimeRemainingScaled(40);
			int j = 40 - k;
			drawTexturedModalRect(guiLeft + 29, guiTop + 65, 176, 0, 40 - j, 10);
		}

		int k = this.scorchingOven.getCookProgressScaled(24);
		drawTexturedModalRect(guiLeft + 79, guiTop + 34, 176, 10, k + 1, 16);
	}

}
