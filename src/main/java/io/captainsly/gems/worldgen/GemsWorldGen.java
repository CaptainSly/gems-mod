package io.captainsly.gems.worldgen;

import java.util.Random;

import cpw.mods.fml.common.IWorldGenerator;
import io.captainsly.gems.init.Init.ModBlocks;
import net.minecraft.block.Block;
import net.minecraft.world.World;
import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.gen.feature.WorldGenMinable;

public class GemsWorldGen implements IWorldGenerator {

	@Override
	public void generate(Random random, int chunkX, int chunkZ, World world, IChunkProvider chunkGenerator, IChunkProvider chunkProvider) {
		switch (world.provider.dimensionId) {
			case 0:
				generateSurface(world, random, chunkX * 16, chunkZ * 16);
			case 1:
				generateNether(world, random, chunkX * 16, chunkZ * 16);
			case 2:
				generateEnd(world, random, chunkX * 16, chunkZ * 16);
		}
	}

	private void generateSurface(World world, Random rand, int x, int z) {
		this.spawnOre(ModBlocks.sapphireOre, world, rand, x, z, 16, 16, 4+rand.nextInt(6), 25, 38, 100);
		this.spawnOre(ModBlocks.rubyOre, world, rand, x, z, 16, 16, 4+rand.nextInt(6), 25, 38, 100);
		this.spawnOre(ModBlocks.topazOre, world, rand, x, z, 16, 16, 4+rand.nextInt(6), 25, 38, 100);
		this.spawnOre(ModBlocks.amethystOre, world, rand, x, z, 16, 16, 4+rand.nextInt(6), 25, 38, 100);
		this.spawnOre(ModBlocks.spinelOre, world, rand, x, z, 16, 16, 2+rand.nextInt(3), 25, 38, 100);
	}

	private void generateNether(World world, Random rand, int x, int z) {

	}

	private void generateEnd(World world, Random rand, int x, int z) {

	}

	private void spawnOre(Block b, World w, Random r, int x, int z, int mX, int mZ, int mvs, int cts, int minY, int maxY) {
		for (int i = 0; i < cts; i++) {
			int posX = x + r.nextInt(mX);
			int posY = minY + r.nextInt(maxY - minY);
			int posZ = z + r.nextInt(mZ);
			(new WorldGenMinable(b, mvs)).generate(w, r, posX, posY, posZ);
		}
	}
	
}
