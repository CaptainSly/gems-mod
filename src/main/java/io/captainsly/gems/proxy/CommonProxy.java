package io.captainsly.gems.proxy;

import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import io.captainsly.gems.Gems;
import io.captainsly.gems.handler.GuiHandler;
import io.captainsly.gems.init.Init.ModBlocks;
import io.captainsly.gems.init.Init.ModCrafting;
import io.captainsly.gems.init.Init.ModEntities;
import io.captainsly.gems.init.Init.ModItems;
import io.captainsly.gems.worldgen.GemsWorldGen;

public class CommonProxy {
	
	public void preInit(FMLPreInitializationEvent e) {
		ModBlocks.init();
		ModItems.init();
		
		GameRegistry.registerWorldGenerator(new GemsWorldGen(), 0);
	}

	public void init(FMLInitializationEvent e) {
		NetworkRegistry.INSTANCE.registerGuiHandler(Gems.instance, new GuiHandler());
		ModEntities.init();
		ModCrafting.init();
	}
	
	public void postInit(FMLPostInitializationEvent e) {}
	
	
}
